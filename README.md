# Pharma Review Classifier

### Introduction
The data for this assignment contains patient reviews on a wide range of drugs within the data from the UCI Machine Learning data repository. The data contains review text, their relevant condition and the outcome of their review in the form of a rating 1-10, with 10 being the most positive. The reviews were submitted by users (patients) of the drugs. Reviews and ratings are grouped into reports on the three aspects: benefits, side effects and overall comment.Objective

The high level objectives in this report is to:
* Perform a Exploratory Data Analysis (EDA) on all the data
* Perform feature engineering on the data set
* Train classification models using the training data
* Test the best models and compare the results to the literature
* Perform a model diagnostics to gain further insightsDraw conclusions and discuss
